/// NOTE that some pdf files cannot be identified from name alone, especially papers predating 2007:
/// https://arxiv.org/help/arxiv_identifier
///
/// Example for ambiguity:
///
/// arXiv:astro-ph/0601622 -> Deducing the Lifetime of Short Gamma-Ray Burst Progenitors from Host Galaxy Demography
/// arXiv:cond-mat/0601622 -> Comment on "Froehlich Mass in GaAs-Based Structures"
/// arXiv:math/0601622 -> Structure Theorem for (d,g,h)-Maps
///
/// I will just assume 'math' as archive
///
use reqwest as rq;
use std::path::PathBuf;
use structopt::StructOpt;
// #[macro_use]
// extern crate arxiv;
use arxiv::bibtex::*;
use arxiv::query::*;
use arxiv::query_result::*;

#[derive(StructOpt)]
#[structopt(name = "arxiv bib generator")]
struct Opt {
    #[structopt(name = "FILES", parse(from_os_str))]
    files: Vec<PathBuf>,
}

#[tokio::main]
async fn main() -> Result<(), rq::Error> {
    let mut id_list = vec![];
    for file in Opt::from_args().files {
        let id: String = file
            .file_stem()
            .and_then(|s| s.to_str())
            .unwrap_or("")
            .into();
        if id.contains('.') {
            id_list.push(Identifier { name: id });
        } else {
            id_list.push(Identifier {
                name: format!("math/{}", id),
            });
        }
    }
    execute_query(&Query::default().with_id_list(id_list)).await
}

async fn execute_query(query: &Query) -> Result<(), rq::Error> {
    let feed = rq::get(
        rq::Url::parse_with_params("http://export.arxiv.org/api/query", query.to_params())
            .expect("Url::parse_with_params"),
    )
    .await?
    .text()
    .await?;
    for entry in QueryResult::parse(&feed)
        .expect("QueryResult::parse")
        .entries
    {
        println!("{}", format_entry(&entry));
    }
    Ok(())
}
