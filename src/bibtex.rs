use crate::entry::*;
use crate::constants::STOPWORDS;

pub fn format_entry(entry: &Entry) -> String {
    use chrono::*;
    let mut ans = "@misc {".to_owned();
    ans.push_str(&format!(
        "{}{}{}",
        short_authors_string(&entry.authors),
        entry.published.year(),
        short_title_string(&entry.title)
    ));
    ans.push_str(&format!(",\n    title={{{}}}", entry.title));
    // TODO support multiple authors
    ans.push_str(&format!(
        ",\n    author={{{}}}",
        entry
            .authors
            .iter()
            .map(|a| a.name.clone())
            .collect::<Vec<_>>()
            .join(" AND ")
    ));
    ans.push_str(&format!(",\n    year={{{}}}", entry.published.year()));
    ans.push_str(&format!(",\n    eprint={{arXiv:{}}}", entry.id));
    ans.push_str(",\n    archivePrefix={arXiv}");
    ans.push_str(&format!(
        ",\n    primaryClass={{{}}}",
        entry.primary_category
    ));
    if let Some(journal_ref) = entry.journal_ref.clone() {
        ans.push_str(&format!(",\n    howpublished={{{}}}", journal_ref));
    }
    if let Some(doi) = entry.doi.clone() {
        ans.push_str(&format!(",\n    doi={{{}}}", doi));
    }
    ans.push_str("\n}\n");
    ans
}

fn short_authors_string(authors: &[Author]) -> String {
    authors
        .get(0)
        .and_then(|a| {
            let lower = a.name.to_lowercase();
            lower.split(' ').last().map(|s| s.to_owned())
        })
        .unwrap_or_else(String::new)
}

fn short_title_string(title: &str) -> String {
    title
        .replace("$", "")
        .replace("'", "")
        .replace("'", "")
        .split(' ')
        .map(|w| w.to_lowercase())
        .find(|w| !STOPWORDS.contains(&w.as_ref()))
        .unwrap_or_else(String::new)
}
