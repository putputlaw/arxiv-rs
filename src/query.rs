/// Reference: https://arxiv.org/help/api/user-manual#_query_interface
#[derive(Clone, Debug)]
pub struct Query {
    pub filter: Filter,
    pub id_list: Vec<Identifier>,
    pub start: usize,
    pub max_results: usize,
    pub sort_by: SortBy,
    pub sort_order: SortOrder,
}

impl Default for Query {
    fn default() -> Self {
        Query {
            filter: Filter::Empty,
            id_list: vec![],
            start: 0,
            max_results: 10,
            sort_by: SortBy::Relevance,
            sort_order: SortOrder::Descending,
        }
    }
}

impl Query {
    pub fn new(filter: Filter, id_list: Vec<Identifier>) -> Self {
        Self::default().with_filter(filter).with_id_list(id_list)
    }

    pub fn with_filter(mut self, filter: Filter) -> Self {
        self.filter = filter;
        self
    }

    pub fn with_id_list(mut self, id_list: Vec<Identifier>) -> Self {
        self.id_list = id_list;
        self
    }

    /// https://arxiv.org/help/api/user-manual#paging
    pub fn at_page(mut self, page: usize) -> Self {
        self.start = page * self.max_results;
        self
    }

    pub fn with_max_results(mut self, max_results: usize) -> Self {
        // https://arxiv.org/help/api/user-manual#paging
        assert!(max_results <= 2000);
        self.max_results = max_results;
        self
    }

    pub fn to_params(&self) -> Vec<(String, String)> {
        let mut ans = vec![];
        ans.push(("search_query".to_owned(), self.filter.to_query_string()));
        ans.push((
            "id_list".to_owned(),
            self.id_list
                .iter()
                .map(|id| id.name.clone())
                .collect::<Vec<_>>()
                .join(","),
        ));
        ans.push(("start".to_owned(), format!("{}", self.start)));
        ans.push(("max_results".to_owned(), format!("{}", self.max_results)));
        ans.push(("sort_by".to_owned(), self.sort_by.to_query_string()));
        ans.push(("sort_order".to_owned(), self.sort_order.to_query_string()));
        ans
    }
}

#[derive(Clone, Debug)]
pub struct Identifier {
    pub name: String,
    // TODO handle version suffix?
    // pub version: String,
}

impl Identifier {
    pub fn new(name: &str) -> Identifier {
        Identifier {
            name: name.to_owned(),
        }
    }
}

impl From<&str> for Identifier {
    fn from(name: &str) -> Self {
        Self::new(name)
    }
}

/// Reference: https://arxiv.org/help/api/user-manual#sort
#[derive(Clone, Copy, Debug)]
pub enum SortBy {
    Relevance,
    LastUpdatedDate,
    SubmittedDate,
}

impl SortBy {
    pub fn to_query_string(self) -> String {
        match self {
            SortBy::Relevance => "relevance",
            SortBy::LastUpdatedDate => "lastUpdatedDate",
            SortBy::SubmittedDate => "submittedDate",
        }
        .into()
    }
}

/// Reference: https://arxiv.org/help/api/user-manual#sort
#[derive(Clone, Copy, Debug)]
pub enum SortOrder {
    Ascending,
    Descending,
}

impl SortOrder {
    pub fn to_query_string(self) -> String {
        match self {
            SortOrder::Ascending => "ascending",
            SortOrder::Descending => "descending",
        }
        .into()
    }
}

/// also referenced as "search query"
/// https://arxiv.org/help/api/user-manual#query_details
#[derive(Clone, Debug)]
pub enum Filter {
    Empty,
    Field(Field, QueryString),
    And(Box<Filter>, Box<Filter>),
    Or(Box<Filter>, Box<Filter>),
    AndNot(Box<Filter>, Box<Filter>),
}

#[derive(Clone, Debug)]
pub struct QueryString(String);

impl QueryString {
    pub fn new(query: &str) -> Self {
        // TODO check string validity
        // TODO consider malicious inputs,
        // TODO allow non-latin characters, accents, etc
        Self(
            query
                .chars()
                .map(|ch| match ch {
                    ' ' => '+',
                    _ => ch,
                })
                .collect(),
        )
    }
}

impl Filter {
    pub fn to_query_string(&self) -> String {
        match self {
            Filter::Empty => "()".into(),
            Filter::Field(field, query) => format!("{}:{}", field.to_query_string(), query.0),
            Filter::And(f1, f2) => format!(
                "{}+AND+{}",
                f1.as_ref().to_query_string(),
                f2.as_ref().to_query_string()
            ),
            Filter::Or(f1, f2) => format!(
                "{}+OR+{}",
                f1.as_ref().to_query_string(),
                f2.as_ref().to_query_string()
            ),
            Filter::AndNot(f1, f2) => format!(
                "{}+ANDNOT+{}",
                f1.as_ref().to_query_string(),
                f2.as_ref().to_query_string()
            ),
        }
    }

    pub fn empty() -> Self {
        Self::Empty
    }

    pub fn field(field: Field, query: &str) -> Self {
        Self::Field(field, QueryString::new(query))
    }

    pub fn title(input: &str) -> Self {
        Self::field(Field::Title, input)
    }

    pub fn author(input: &str) -> Self {
        Self::field(Field::Author, input)
    }

    pub fn abstract_(input: &str) -> Self {
        Self::field(Field::Abstract, input)
    }

    pub fn comment(input: &str) -> Self {
        Self::field(Field::Comment, input)
    }

    pub fn journal_reference(input: &str) -> Self {
        Self::field(Field::JournalReference, input)
    }

    pub fn subject_category(input: &str) -> Self {
        Self::field(Field::SubjectCategory, input)
    }

    pub fn report_number(input: &str) -> Self {
        Self::field(Field::ReportNumber, input)
    }

    pub fn all(input: &str) -> Self {
        Self::field(Field::All, input)
    }

    pub fn and(filter1: Filter, filter2: Filter) -> Self {
        Self::And(Box::new(filter1), Box::new(filter2))
    }

    pub fn or(filter1: Filter, filter2: Filter) -> Self {
        Self::Or(Box::new(filter1), Box::new(filter2))
    }

    pub fn and_not(filter1: Filter, filter2: Filter) -> Self {
        Self::AndNot(Box::new(filter1), Box::new(filter2))
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Field {
    Title,
    Author,
    Abstract,
    Comment,
    JournalReference,
    SubjectCategory,
    ReportNumber,
    // Id,              (use id_list instead)
    All,
}

impl Field {
    pub fn to_query_string(self) -> String {
        match self {
            Field::Title => "ti",
            Field::Author => "au",
            Field::Abstract => "abs",
            Field::Comment => "co",
            Field::JournalReference => "jr",
            Field::SubjectCategory => "cat",
            Field::ReportNumber => "rn",
            // Field::Id => "id",
            Field::All => "all",
        }
        .into()
    }
}

// TODO figure out macros, in ptic `and/or/andnot`
//
#[macro_export]
macro_rules! q {
    (ti: $var:expr) => {
        Filter::title($var)
    };
    (au: $var:expr) => {
        Filter::author($var)
    };
    (abs: $var:expr) => {
        Filter::author($var)
    };
    (co: $var:expr) => {
        Filter::comment($var)
    };
    (jr: $var:expr) => {
        Filter::journal_reference($var)
    };
    (cat: $var:expr) => {
        Filter::subject_category($var)
    };
    (rn: $var:expr) => {
        Filter::report_number($var)
    };
    (all: $var:expr) => {
        Filter::all($var)
    };
}
