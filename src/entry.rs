use crate::constants::*;

// TODO
// 1. replace String with custom types (e.g. id: String -> id: Id)

/// Reference: https://arxiv.org/help/api/user-manual#_details_of_atom_results_returned
#[derive(Clone, Debug)]
pub struct Entry {
    /// The title of the article.
    pub title: String,
    /// A url http://arxiv.org/abs/id
    pub id: String,
    /// The date that version 1 of the article was submitted.
    pub published: DateTime,
    /// The date that the retrieved version of the article was submitted. Same as <published> if the retrieved version is version 1.
    pub updated: DateTime,
    /// The article abstract.
    pub summary: String,
    /// One for each author. Has child element <name> containing the author name.
    pub authors: Vec<Author>,
    /// Can be up to 3 given url's associated with this article.
    pub links: Vec<Link>,
    /// The arXiv or ACM or MSC category for an article if present.
    pub categories: Vec<String>,
    /// The primary arXiv category.
    pub primary_category: String,
    /// The authors comment if present.
    pub comment: Option<String>,
    /// A journal reference if present.
    pub journal_ref: Option<String>,
    /// A url for the resolved DOI to an external resource if present.
    pub doi: Option<String>,
}

pub type DateTime = chrono::DateTime<chrono::FixedOffset>;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Author {
    pub name: String,
    /// The author's affiliation included as a subelement of <author> if present.
    pub affiliation: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Link {
    pub title: Option<String>,
    pub rel: String,
    pub typ: Option<String>,
    pub href: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Subject(String);

impl Subject {
    pub fn new(name: &str) -> Result<Self, String> {
        if SUBJECTS.contains(&name) {
            Ok(Self(name.to_owned()))
        } else {
            Err(format!("{:?} is not a recognized subject classifier", name))
        }
    }

    /// will not verify validity of subject classifier
    pub fn unchecked_new(name: &str) -> Self {
        Self(name.to_owned())
    }
}
