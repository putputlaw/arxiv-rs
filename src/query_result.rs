use crate::entry::*;

#[derive(Clone, Debug)]
pub struct QueryResult {
    pub title: String,
    pub id: String,
    pub updated: DateTime,
    pub link: Link,
    pub pagination: Pagination,
    pub entries: Vec<Entry>,
}

impl QueryResult {
    pub fn parse(feed: &str) -> Option<Self> {
        use parsing::*;

        use xml::reader::{EventReader, XmlEvent};
        let mut state = ParseState::Init;
        let parser = EventReader::from_str(feed);

        let mut title = None;
        let mut id = None;
        let mut updated = None;
        let mut link = None;
        let mut total_results = None;
        let mut start_index = None;
        let mut items_per_page = None;
        let mut entries = vec![];
        let mut entry = Entry_::default();
        let mut author = Author_::default();
        for e in parser {
            match e {
                Ok(XmlEvent::StartElement {
                    name, attributes, ..
                }) => match (state, name.local_name.as_ref()) {
                    (ParseState::Init, "feed") => {
                        state = ParseState::Feed;
                    }
                    (ParseState::Feed, "title") => {
                        state = ParseState::FeedTitle;
                    }
                    (ParseState::Feed, "id") => {
                        state = ParseState::FeedId;
                    }
                    (ParseState::Feed, "updated") => {
                        state = ParseState::FeedUpdated;
                    }
                    (ParseState::Feed, "link") => {
                        state = ParseState::FeedLink;
                        let mut title = None;
                        let mut rel = None;
                        let mut typ = None;
                        let mut href = None;
                        for attr in attributes {
                            match attr.name.local_name.as_ref() {
                                "title" => title = Some(attr.value.clone()),
                                "rel" => rel = Some(attr.value.clone()),
                                "type" => typ = Some(attr.value.clone()),
                                "href" => href = Some(attr.value.clone()),
                                _ => {}
                            }
                        }
                        link = Some(Link {
                            title,
                            rel: rel?,
                            typ,
                            href: href?,
                        });
                    }
                    (ParseState::Feed, "totalResults") => {
                        state = ParseState::FeedTotalResults;
                    }
                    (ParseState::Feed, "startIndex") => {
                        state = ParseState::FeedStartIndex;
                    }
                    (ParseState::Feed, "itemsPerPage") => {
                        state = ParseState::FeedItemsPerPage;
                    }
                    (ParseState::Feed, "entry") => {
                        state = ParseState::Entry;
                    }
                    (ParseState::Entry, "title") => {
                        state = ParseState::EntryTitle;
                    }
                    (ParseState::Entry, "id") => {
                        state = ParseState::EntryId;
                    }
                    (ParseState::Entry, "published") => {
                        state = ParseState::EntryPublished;
                    }
                    (ParseState::Entry, "updated") => {
                        state = ParseState::EntryUpdated;
                    }
                    (ParseState::Entry, "summary") => {
                        state = ParseState::EntrySummary;
                    }
                    (ParseState::Entry, "comment") => {
                        state = ParseState::EntryComment;
                    }
                    (ParseState::Entry, "journal_ref") => {
                        state = ParseState::EntryJournal;
                    }
                    (ParseState::Entry, "doi") => {
                        state = ParseState::EntryDOI;
                    }
                    (ParseState::Entry, "primary_category") => {
                        state = ParseState::EntryPrimCategory;
                        for attr in attributes {
                            if attr.name.local_name == "term" {
                                entry.primary_category = Some(attr.value.to_owned());
                                break;
                            }
                        }
                    }
                    (ParseState::Entry, "author") => {
                        state = ParseState::EntryAuthor;
                    }
                    (ParseState::EntryAuthor, "name") => {
                        state = ParseState::EntryAuthorName;
                    }
                    (ParseState::EntryAuthor, "affiliation") => {
                        state = ParseState::EntryAuthorAffiliation;
                    }
                    (ParseState::Entry, "link") => {
                        state = ParseState::EntryLink;
                        let mut title = None;
                        let mut rel = None;
                        let mut typ = None;
                        let mut href = None;
                        for attr in attributes {
                            match attr.name.local_name.as_ref() {
                                "title" => title = Some(attr.value.clone()),
                                "rel" => rel = Some(attr.value.clone()),
                                "type" => typ = Some(attr.value.clone()),
                                "href" => href = Some(attr.value.clone()),
                                _ => {}
                            }
                        }
                        entry.links.push(Link {
                            title,
                            rel: rel?,
                            typ,
                            href: href?,
                        });
                    }
                    (ParseState::Entry, "category") => {
                        state = ParseState::EntryCategory;
                        for attr in attributes {
                            if attr.name.local_name == "term" {
                                entry.categories.push(attr.value.to_owned());
                                break;
                            }
                        }
                    }
                    _ => {}
                },
                Ok(XmlEvent::EndElement { name, .. }) => match (state, name.local_name.as_ref()) {
                    (ParseState::Feed, "feed") => {
                        break;
                    }
                    (ParseState::FeedTitle, "title") => state = ParseState::Feed,
                    (ParseState::FeedId, "id") => state = ParseState::Feed,
                    (ParseState::FeedUpdated, "updated") => state = ParseState::Feed,
                    (ParseState::FeedLink, "link") => state = ParseState::Feed,
                    (ParseState::FeedTotalResults, "totalResults") => state = ParseState::Feed,
                    (ParseState::FeedStartIndex, "startIndex") => state = ParseState::Feed,
                    (ParseState::FeedItemsPerPage, "itemsPerPage") => state = ParseState::Feed,
                    (ParseState::Entry, "entry") => {
                        let Entry_ {
                            title,
                            id,
                            published,
                            updated,
                            summary,
                            comment,
                            journal_ref,
                            doi,
                            primary_category,
                            authors,
                            links,
                            categories,
                        } = std::mem::replace(&mut entry, Entry_::default());
                        let id = id.and_then(|s| s.split('/').last().map(|i| i.to_owned()));
                        entries.push(Entry {
                            title: title?,
                            id: id?,
                            published: published?,
                            updated: updated?,
                            summary: summary?,
                            authors,
                            links,
                            categories,
                            primary_category: primary_category?,
                            comment,
                            journal_ref,
                            doi,
                        });
                        state = ParseState::Feed;
                    }
                    (ParseState::EntryTitle, "title") => state = ParseState::Entry,
                    (ParseState::EntryId, "id") => state = ParseState::Entry,
                    (ParseState::EntryPublished, "published") => state = ParseState::Entry,
                    (ParseState::EntryUpdated, "updated") => state = ParseState::Entry,
                    (ParseState::EntrySummary, "summary") => state = ParseState::Entry,
                    (ParseState::EntryComment, "comment") => state = ParseState::Entry,
                    (ParseState::EntryJournal, "journal_ref") => state = ParseState::Entry,
                    (ParseState::EntryDOI, "doi") => state = ParseState::Entry,
                    (ParseState::EntryPrimCategory, "primary_category") => {
                        state = ParseState::Entry
                    }
                    (ParseState::EntryAuthor, "author") => {
                        let Author_ { name, affiliation } =
                            std::mem::replace(&mut author, Author_::default());
                        entry.authors.push(Author {
                            name: name?,
                            affiliation,
                        });
                        state = ParseState::Entry
                    }
                    (ParseState::EntryAuthorName, "name") => state = ParseState::EntryAuthor,
                    (ParseState::EntryAuthorAffiliation, "affiliation") => {
                        state = ParseState::EntryAuthor
                    }
                    (ParseState::EntryLink, "link") => state = ParseState::Entry,
                    (ParseState::EntryCategory, "category") => state = ParseState::Entry,
                    _ => {}
                },
                Ok(XmlEvent::Characters(c)) => match state {
                    ParseState::FeedTitle => {
                        title = Some(c);
                    }
                    ParseState::FeedId => {
                        id = Some(c);
                    }
                    ParseState::FeedUpdated => {
                        updated = chrono::DateTime::parse_from_rfc3339(&c).ok();
                    }
                    ParseState::FeedTotalResults => {
                        total_results = c.trim().parse::<usize>().ok();
                    }
                    ParseState::FeedStartIndex => {
                        start_index = c.trim().parse::<usize>().ok();
                    }
                    ParseState::FeedItemsPerPage => {
                        items_per_page = c.trim().parse::<usize>().ok();
                    }
                    ParseState::EntryTitle => {
                        entry.title = Some(c);
                    }
                    ParseState::EntryId => {
                        entry.id = Some(c);
                    }
                    ParseState::EntryPublished => {
                        entry.published = chrono::DateTime::parse_from_rfc3339(&c).ok();
                    }
                    ParseState::EntryUpdated => {
                        entry.updated = chrono::DateTime::parse_from_rfc3339(&c).ok();
                    }
                    ParseState::EntrySummary => {
                        entry.summary = Some(c);
                    }
                    ParseState::EntryComment => {
                        entry.comment = Some(c);
                    }
                    ParseState::EntryJournal => {
                        entry.journal_ref = Some(c);
                    }
                    ParseState::EntryDOI => {
                        entry.doi = Some(c);
                    }
                    ParseState::EntryAuthorName => {
                        author.name = Some(c);
                    }
                    ParseState::EntryAuthorAffiliation => {
                        author.affiliation = Some(c);
                    }
                    _ => {}
                },
                Err(e) => {
                    panic!("Error: {}", e);
                }
                _ => {}
            }
        }

        Some(QueryResult {
            title: title?,
            id: id?,
            updated: updated?,
            link: link?,
            pagination: Pagination {
                num_items: total_results?,
                items_per_page: items_per_page?,
                page: start_index?,
            },
            entries,
        })
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Pagination {
    num_items: usize,
    items_per_page: usize,
    page: usize,
}

impl Pagination {
    pub fn new(num_items: usize, items_per_page: usize) -> Self {
        Self {
            num_items,
            items_per_page,
            page: 0,
        }
    }

    pub fn go_to_page(mut self, page: usize) -> Self {
        self.page = page;
        self
    }

    pub fn go_to_index(mut self, item_index: usize) -> Option<Self> {
        if item_index < self.num_items {
            self.page = self.page_of(item_index);
            Some(self)
        } else {
            None
        }
    }

    pub fn previous_page(mut self) -> Option<Self> {
        if self.page > 0 {
            self.page -= 1;
            Some(self)
        } else {
            None
        }
    }

    pub fn next_page(mut self) -> Option<Self> {
        if self.num_items > 0 && self.page < self.page_of(self.num_items - 1) {
            self.page += 1;
            Some(self)
        } else {
            None
        }
    }

    pub fn page_of(self, item_index: usize) -> usize {
        item_index / self.items_per_page
    }

    pub fn num_items(self) -> usize {
        self.num_items
    }

    pub fn page(self) -> usize {
        self.page
    }

    pub fn items_per_page(self) -> usize {
        self.items_per_page
    }
}

mod parsing {
    use crate::entry::*;

    #[derive(Clone, Debug)]
    pub struct Entry_ {
        pub title: Option<String>,
        pub id: Option<String>,
        pub published: Option<DateTime>,
        pub updated: Option<DateTime>,
        pub summary: Option<String>,
        pub comment: Option<String>,
        pub journal_ref: Option<String>,
        pub doi: Option<String>,
        pub primary_category: Option<String>,
        pub authors: Vec<Author>,
        pub links: Vec<Link>,
        pub categories: Vec<String>,
    }

    impl Default for Entry_ {
        fn default() -> Self {
            Self {
                title: None,
                id: None,
                published: None,
                updated: None,
                summary: None,
                comment: None,
                journal_ref: None,
                doi: None,
                primary_category: None,
                authors: vec![],
                links: vec![],
                categories: vec![],
            }
        }
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Author_ {
        pub name: Option<String>,
        pub affiliation: Option<String>,
    }

    impl Default for Author_ {
        fn default() -> Self {
            Self {
                name: None,
                affiliation: None,
            }
        }
    }

    #[derive(Clone, Copy, Debug)]
    pub enum ParseState {
        Init,
        Feed,
        FeedTitle,
        FeedId,
        FeedUpdated,
        FeedLink,
        FeedTotalResults,
        FeedStartIndex,
        FeedItemsPerPage,
        Entry,
        EntryTitle,
        EntryId,
        EntryPublished,
        EntryUpdated,
        EntrySummary,
        EntryComment,
        EntryJournal,
        EntryDOI,
        EntryPrimCategory,
        EntryAuthor,
        EntryAuthorName,
        EntryAuthorAffiliation,
        EntryLink,
        EntryCategory,
    }
}
